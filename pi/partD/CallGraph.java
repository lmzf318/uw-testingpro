import java.util.*;

/**
 * Created by Yuning on 3/4/16.
 * <p/>
 * To store input call graphs and generate support and confidence
 */
public class CallGraph {
    private final static String TAG = "CallGraph";

    // For every function, add a HashMap into this Map after processing all functions.
    private Map<String, HashMap<String, HashSet<String>>> cgMapCollection = new HashMap<>();
    // for every call, scope it the key, HashMap<String, HashSet<String>> is call-{lines}
    // line count from 0: first line is 0.
    private Map<String, HashMap<String, HashSet<String>>> callLineCollection = new HashMap<>();

    // temporarily store a method's scopes
    private Map<String, HashSet<String>> bugMap = new HashMap<>();

    public CallGraph() {
        DiffUtil.log(TAG, "constructor");
    }

    public void addFunctionMap(String scope, HashMap<String, HashSet<String>> map) {
        this.cgMapCollection.put(scope, map);
    }

    public void addLineMap(String scope, HashMap<String, HashSet<String>> map) {
        this.callLineCollection.put(scope, map);
    }

    public HashSet<String> getScopeList(String method) {
        return this.bugMap.get(method);
    }

    /**
     * Store (maybe) bug method and its corresponding scope in list.
     * While necessary, loop this list to print all scopes of this method
     */
    private void updateBugMap(String method, String scope) {
        HashSet<String> bugSet = null;
        if (bugMap.containsKey(method)) {
            bugSet = bugMap.get(method);
        } else {
            bugSet = new HashSet<>();
        }

        bugSet.add(scope);
        DiffUtil.log("Update BugMap", "method = " + method + ", scope = " + scope);
        this.bugMap.put(method, bugSet);
    }

    /**
     * add a scope's call set to a queue
     */
    private void addCall2Queue(LinkedList<String> queue, HashMap<String, HashSet<String>> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            queue.offer((String) entry.getKey());
        }
    }

    /**
     * Pairs like (A,B) are stored in pairMap.
     * Key is one call.
     * Value is set of this call's pair methods.
     */
    private void addCall2Map(String call, LinkedList<String> queue, HashMap<String, HashSet<String>> pairMap) {
        for (String pair : queue) {
            DiffUtil.log("Generate Pairs", "call = " + call + ", pair = " + pair);
            DiffUtil.log("Generate Pairs", "pairMap.get(call) = " + pairMap.get(call));
            DiffUtil.log("Generate Pairs", "pairMap.get(pair) = " + pairMap.get(pair));
            //if (DiffUtil.equals(pairMap.get(call), pair) || DiffUtil.equals(pairMap.get(pair), call)) {
            if ((pairMap.get(call) != null && pairMap.get(call).contains(pair))
                    || (pairMap.get(pair) != null && pairMap.get(pair).contains(call))) {
                DiffUtil.log("Generate Pairs", "pass!");
                continue;
            }

            HashSet<String> pairSet = pairMap.get(call);
            if (pairSet == null) {
                pairSet = new HashSet<>();
                pairMap.put(call, pairSet);
            }

            pairSet.add(pair);
        }
    }

    /**
     * add pairs to a map<call, calls>
     * For example, [A, B, C] in a scope is saved as <A={B, C}, B={C}>, which means pairs are A*{B,C} and B*{C}
     * For p-1-d, one solution ony cares about data dependent edge between two calls
     */
    public void generatePairMap(HashMap<String, HashSet<String>> pairMap) {
        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            HashMap<String, HashSet<String>> map = (HashMap<String, HashSet<String>>) entry.getValue();
            LinkedList<String> queue = new LinkedList<>();
            addCall2Queue(queue, map);
            DiffUtil.log("Queue Debug", queue.toString());
            for (String currentCall = queue.poll(); currentCall != null; currentCall = queue.poll()) {
                addCall2Map(currentCall, queue, pairMap);
            }
        }
    }

    /**
     * Traverse the whole call graph and find a)method, b)methodPair and c)pair
     * For p-1-a
     * 'checkDD' for p-1-d
     *
     * @param method     target method
     * @param methodPair method pair
     * @return a Pair with count of a, b and c, which means the appearance of method, methodPair, and pair
     */
    public Pair findBugData(String method, String methodPair) {
        // clear scopes every time finding a new bug
        // to eliminate previous result
        if (bugMap.containsKey(method)) {
            bugMap.remove(method);
        }
        if (bugMap.containsKey(methodPair)) {
            bugMap.remove(methodPair);
        }

        Pair pair = new Pair();
        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();

            HashMap<String, HashSet<String>> map = (HashMap<String, HashSet<String>>) entry.getValue();
            String scope = (String) entry.getKey();
            if (map.containsKey(method) && !map.containsKey(methodPair)) {
                pair.incrementSingelA();

                updateBugMap(method, scope);
            } else if (!map.containsKey(method) && map.containsKey(methodPair)) {
                pair.incrementSingelB();

                updateBugMap(methodPair, scope);
            } else if (map.containsKey(method) && map.containsKey(methodPair)) {
                // in s scope, only dd pair be treated as a valid pair
                if (checkDD(map, method, methodPair, scope)) {
                    pair.incrementPair();
                }
            }
        }
        return pair;
    }

    // check whether the two calls have data dependence
    // check if intersection variable changed in the paht from call to pair
    private boolean checkDD(HashMap<String, HashSet<String>> rootMap, String call, String pair, String scope) {
        // check data dependency
        HashSet<String> callArg = rootMap.get(call);
        HashSet<String> pairArg = rootMap.get(pair);
        HashSet<String> intersection = new HashSet<>(callArg);
        // get intersection
        intersection.retainAll(pairArg);

        if ((!callArg.isEmpty() || !pairArg.isEmpty()) && intersection.isEmpty()) {
            // no intersection means no data dependency
            // at least one call should have arguments
            return false;
        } else {
            // 1. both call have no arguments
            // 2. the two call have intersection

            if (intersection.isEmpty() || assignmentsUnmodified(call, pair, scope, intersection)) {
                return true;
            } else {
                return false;
            }
        }
    }

    // call this method means a pair has been found in this scope
    private boolean assignmentsUnmodified(String call, String pair, String scope, HashSet<String> intersection) {
        DiffUtil.log("SortList", "Scope: " + scope);
        DiffUtil.log("SortList", "pair: " + call + "-" + pair);
        boolean ret = false;
        ArrayList<LineObj> fullList = new ArrayList<>();

        // add content-line to list for sort
        HashMap<String, HashSet<String>> fullMap = callLineCollection.get(scope);
        Iterator it = fullMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            // call/variable
            String content = (String) entry.getKey();
            // line numbers
            HashSet<String> set = (HashSet<String>) entry.getValue();
            for (Iterator setIt = set.iterator(); setIt.hasNext(); ) {
                String lineStr = (String) setIt.next();
                fullList.add(new LineObj(content, lineStr));
            }
        }

        Collections.sort(fullList);
        DiffUtil.log("SortList", fullList.toString());

        // store call or pair
        String firstFind = null;
        HashSet<String> assignTemp = new HashSet<>();
        for (LineObj lineObj : fullList) {
            if (ret) {
                // there is a path that intersected variables have not been assigned
                break;
            }

            // current line is a call to 'call' or 'pair', start point
            if (DiffUtil.isEmpty(firstFind) &&
                    (DiffUtil.equals(lineObj.getName(), Constant.CALL_PREFIX + call) ||
                            DiffUtil.equals(lineObj.getName(), Constant.CALL_PREFIX + pair))) {
                firstFind = lineObj.getName();

                // init temporary store data
                assignTemp.clear();
            }

            // start point has been found and end point has not
            else if (!DiffUtil.isEmpty(firstFind)) {
                if (lineObj.getName().contains(Constant.ASSIGN_PREFIX)) {
                    // an assignment between two calls
                    // save assignment, without prefix!!!
                    // TODO remove this line to generate a full set
                    // only store var in intersection
                    String var = lineObj.getName().substring(Constant.ASSIGN_PREFIX.length());
                    if (intersection.contains(var)) {
                        assignTemp.add(var);
                    }
                } else if (DiffUtil.equals(lineObj.getName(), Constant.CALL_PREFIX + call) ||
                        DiffUtil.equals(lineObj.getName(), Constant.CALL_PREFIX + pair)) {
                    // this call equals to 'call' or 'pair'
                    if (DiffUtil.equals(lineObj.getName(), firstFind)) {
                        // equals to the first call, re store assignments and clear previous found ones
                        firstFind = lineObj.getName();

                        // clear temporary store data
                        assignTemp.clear();
                    } else {
                        // another call has been found, end point
                        // compare with intersection to see if all var in intersection has been assigned
                        DiffUtil.log("SortList", "assignTemp: " + assignTemp);
                        DiffUtil.log("SortList", "intersection: " + intersection);
                        ret = (assignTemp.size() != intersection.size());

                        // set findFind to null for next round
                        firstFind = null;
                    }
                }
            }
        }

        return ret;
    }

    @Override
    public String toString() {
        return "CallGraph{" +
                "cgMapCollection=" + cgMapCollection +
                ", callLineCollection=" + callLineCollection +
                ", bugMap=" + bugMap +
                '}';
    }
}
