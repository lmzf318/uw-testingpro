#include "stdio.h"

void scope1();
void scope2();
void scope3();
void scope4();
void scope5();
void scope6();

void A(int a);
void B(int b1, int b2);
void C();
void D(int d);



int main() {
  scope1();
  scope2();
  scope3();
  scope4();
  scope5();
  scope6();
  
  return 0;
}

void scope1() {
  int i = 0;
  int j = 0;
  A(j);
  B(i, i);
  C();
  if(j==0)
  {
     int i = 0;
     D(i);
  }
}

void scope2() {
  int i = 0;
  A(i);
  C();
  D(i);
}

void scope3() {
  int i = 0, j =0;
  A(i);
  i++;
  i = 3;
  B(i, 1);
}

void scope4() {
  B(1, 1);
  D(1);
  scope1();
}

void scope5() {
  int i = 0;
  int j = 1;
  B(j, j);
  D(i);
  A(i);
}

void scope6() {
  B(1, 1);
  D(1);
}

void A(int a) {
  printf("A %d\n", a);
}

void B(int b1, int b2) {
  printf("B\n");
}

void C() {
  printf("C\n");
}

void D(int d) {
  printf("D\n");
}

