import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by Yuning on 3/3/16.
 */
public class DiffUtil {
    private static HashMap<String, HashSet<String>> callCollection = null;
    private static HashMap<String, HashSet<String>> lineCollection = null;
    private static HashMap<String, String> argumentCollection = null;

    private static int lineCounter = 0;

    public static void findBugs(int defaultSupport, float defaultConfidence, HashMap<String, HashSet<String>> pairMap, CallGraph callGraph, int traverseLevel) {
        Iterator it = pairMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            // entry.getKey()
            // entry.getValue()
            String method = (String) entry.getKey();
            HashSet<String> pairSet = (HashSet<String>) entry.getValue();

            for (Iterator setIt = pairSet.iterator(); setIt.hasNext(); ) {
                String methodPair = (String) setIt.next();
                calculateBug(method, methodPair, callGraph, defaultSupport, defaultConfidence);
            }

        }
    }

    private static void calculateBug(String method, String methodPair, CallGraph callGraph,
                                     int defaultSupport, float defaultConfidence) {
        Pair pair = callGraph.findBugData(method, methodPair);
        // for example, for pair(A, B), singleA means number of appearances of A in call graph
        int singleA = pair.getSingleA();
        int singleB = pair.getSingleB();
        int support = pair.getPair();

        float confidenceA = support > 0 ? getConfidence(singleA, support) : 0;
        float confidenceB = support > 0 ? getConfidence(singleB, support) : 0;

        if (singleA > 0 && support >= defaultSupport
                && confidenceA >= defaultConfidence) {
            printBug(support, method, methodPair, callGraph, confidenceA);
        }

        if (singleB > 0 && support >= defaultSupport
                && confidenceB >= defaultConfidence) {
            printBug(support, methodPair, method, callGraph, confidenceB);
        }
    }

    private static float getConfidence(int single, float support) {
        return ((float) (support) / (float) (support + single)) * 100f;
    }

    private static void printBug(int support,
                                 String method, String methodPair, CallGraph callGraph,
                                 float confidence) {
        HashSet<String> scopeSet = callGraph.getScopeList(method);
        for (Iterator scopeIt = scopeSet.iterator(); scopeIt.hasNext(); ) {
            String scope = (String) scopeIt.next();
            DiffUtil.log(method, methodPair, scope, support, confidence);
        }
    }

    public static void readStream(CallGraph callGraph) {
        InputStreamReader isReader = new InputStreamReader(System.in);
        BufferedReader bufReader = new BufferedReader(isReader);

        String inputStr = null;

        try {
            while ((inputStr = bufReader.readLine()) != null) {
                // debug
                //log("bc input", inputStr);
                DiffUtil.handleInput(callGraph, inputStr);
            }
            isReader.close();
        } catch (Exception e) {
            DiffUtil.log("Exception", e.toString());
        }
    }

    /**
     * Analyze input string line by line and add scope/function call to call graph collection.
     *
     * @param callGraph store scope and call
     * @param inputStr  input String
     */
    public static void handleInput(CallGraph callGraph, String inputStr) {
        DiffUtil.log("Input readStream", inputStr);

        // goto a function
        if (inputStr.contains(Constant.NODE) && inputStr.contains(Constant.NODE_SCOPE)) {
            handleScope(inputStr, callGraph);
            return;
        }

        String node = null;

        // call function in a scope
        if (inputStr.contains(Constant.NODE_CALL) && inputStr.contains(Constant.NODE_CALL_POSITION)) {
            node = Constant.CALL_PREFIX + handleCall(node, inputStr);
        }

        // save parameters
        if (inputStr.contains(Constant.NODE_ARG) &&
                inputStr.indexOf(Constant.NODE_ARG_DIV) != inputStr.lastIndexOf(Constant.NODE_ARG_DIV)) {
            handleArguments(inputStr);
        }

        // define variable
        if (inputStr.contains(Constant.NODE_DEF) && inputStr.contains(Constant.NODE_DEF_DIV)) {
            node = Constant.DEF_PREFIX + handleDef(node, inputStr);
        }

        // assign value to a variable
        if (inputStr.contains(Constant.NODE_ASSIGN) && inputStr.contains(Constant.NODE_ASSIGN_DIV)) {
            node = Constant.ASSIGN_PREFIX + handleAssign(node, inputStr);
        }

        DiffUtil.log("Input readStream", "node after handle is " + node);
        DiffUtil.log("Input readStream", "lineCounter after handle is " + lineCounter);

        if (!DiffUtil.isEmpty(node)) {
            HashSet<String> lineSet = null;

            if (lineCollection.containsKey(node)) {
                lineSet = lineCollection.get(node);
            } else {
                lineSet = new HashSet<>();
            }

            lineSet.add(String.valueOf(lineCounter));

            lineCollection.put(node, lineSet);
        }

        lineCounter++;
    }

    private static String handleAssign(String node, String inputStr) {
        int divIndex = inputStr.lastIndexOf(Constant.NODE_ASSIGN_DIV);
        int endIndex = inputStr.lastIndexOf(Constant.NODE_ASSIGN_DIV_END);
        if (endIndex < divIndex) {
            // store i32 %b1, i32* %1, align 4
            // store i32 0, i32* %i, align 4
            node = inputStr.substring(divIndex + Constant.NODE_ASSIGN_DIV_LEN);
        } else {
            // store i32 0, i32* %1
            node = inputStr.substring(divIndex + Constant.NODE_ASSIGN_DIV_LEN, endIndex);
        }

        return node;
    }

    private static String handleDef(String node, String inputStr) {
        // %i = alloca i32, align 4
        node = inputStr.substring(inputStr.indexOf(Constant.NODE_DEF_DIV)
                + Constant.NODE_DEF_DIV_LEN, inputStr.indexOf(Constant.NODE_DEF_DIV_END));

        return node;
    }

    private static void handleScope(String inputStr, CallGraph callGraph) {
        // get scope
        String scope = DiffUtil.getScope(inputStr);
        DiffUtil.log("Input readStream scope", scope);

        // eliminate root scope
        if (scope == null) {
            callCollection = null;
            lineCollection = null;
            argumentCollection = null;
            return;
        }

        // set line to 0
        lineCounter = 0;
        // start to generate a new call graph callCollection
        callCollection = new HashMap<>();
        lineCollection = new HashMap<>();
        // generate a tempotary map to save number - argument pairs
        argumentCollection = new HashMap<>();

        callGraph.addFunctionMap(scope, callCollection);
        callGraph.addLineMap(scope, lineCollection);
    }

    private static String handleCall(String node, String inputStr) {
        // eliminate "(" in call str
        do {
            if (node == null) {
                node = inputStr.substring(inputStr.indexOf(Constant.NODE_CALL_POSITION)
                        + Constant.NODE_CALL_POSITION_OFFSET, inputStr.lastIndexOf(Constant.NODE_CALL_POSITION_END));
            } else {
                node = node.substring(0, node.lastIndexOf(Constant.NODE_CALL_POSITION_END));
            }
        } while (node.contains(Constant.NODE_CALL_POSITION_END));

        DiffUtil.log("Input readStream call", node);
        if (callCollection != null) {
            DiffUtil.log("Put callCollection", node);

            HashSet<String> argSet = null;

            if (callCollection.containsKey(node)) {
                argSet = callCollection.get(node);
            } else {
                argSet = new HashSet<>();
            }

            storeArguments(argSet, inputStr);

            callCollection.put(node, argSet);
        }

        return node;
    }

    private static void handleArguments(String inputStr) {
        DiffUtil.log("arguments", "Handle arguments.");
        String num = inputStr.substring(inputStr.indexOf(Constant.NODE_ARG_DIV)
                + Constant.NODE_ARG_DIV_LEN, inputStr.indexOf(Constant.NODE_ARG_DIV_NUM_END));
        String arg = inputStr.substring(inputStr.lastIndexOf(Constant.NODE_ARG_DIV)
                + Constant.NODE_ARG_DIV_LEN, inputStr.indexOf(Constant.NODE_ARG_DIV_ARG_END));
        DiffUtil.log("arguments", num + "-" + arg);

        if (argumentCollection != null) {
            argumentCollection.put(num, arg);
        }
    }

    /**
     * save arguments in inputStr to this argSet
     */
    private static void storeArguments(HashSet<String> argSet, String inputStr) {
        String temp = inputStr;
        while (temp.contains(Constant.NODE_ARG_DIV)) {
            DiffUtil.log("store arguments", "temp " + temp);
            int divIndex = temp.indexOf(Constant.NODE_ARG_DIV);
            int endIndex = temp.indexOf(Constant.NODE_ARG_DIV_ARG_END);
            int mulEndIndex = temp.lastIndexOf(Constant.NODE_ARG_DIV_MUL_ARG_END);

            String argNum = null;
            if (endIndex > divIndex) {
                // call void @B(i32 %2, i32 %3)
                argNum = temp.substring(divIndex + Constant.NODE_ARG_DIV_LEN, endIndex);
            } else {
                // call void @B(i32 %2) or , i32 %3)
                argNum = temp.substring(divIndex + Constant.NODE_ARG_DIV_LEN, mulEndIndex);
            }
            DiffUtil.log("store arguments", "argNum " + argNum);

            String arg = argumentCollection.get(argNum);
            DiffUtil.log("store arguments", "arg " + arg);
            if (arg != null) {
                argSet.add(arg);
            }

            temp = temp.substring(divIndex + 1);
        }
    }

    public static String getScope(String inputStr) {
        int index = 0;
        if ((index = inputStr.indexOf(Constant.NODE_SCOPE)) > -1) {
            //e.g. Call graph node for function: 'scope5'<<0x178ccc0>>  #uses=2
            return inputStr.substring(index + Constant.NODE_SCOPE_OFFSET,
                    inputStr.lastIndexOf(Constant.NODE_SCOPE_END));
        } else {
            return null;
        }
    }

    /**
     * compare 2 Strings
     * to avoid hashcode collision, only compare Strings if the hashCode is equal
     **/
    public static boolean equals(final String srcStr, final String desStr) {
        return srcStr != null && desStr != null
                && srcStr.length() == desStr.length()
                && srcStr.hashCode() == desStr.hashCode()
                && srcStr.equals(desStr);
    }

    // check if a String is "" or null
    public static boolean isEmpty(String str) {
        return str == null || str.equals("");
    }

    // print result logs
    public static void log(String method, String methodPair, String scope, int support, float confidence) {
        String pairStr = method.compareTo(methodPair) < 0 ?
                (method + ", " + methodPair) : (methodPair + ", " + method);
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        String confidenceStr = df.format(confidence);
        System.out.println("bug: " + method + " in " + scope
                + ", pair: (" + pairStr + "), support: " + support
                + ", confidence: " + confidenceStr + "%");
    }

    // print out logs
    public static void log(String str) {
        //System.out.println(str);
    }

    // print out logs with tag
    public static void log(String tag, String content) {
        //System.out.println(tag + ": " + content);
    }
}
