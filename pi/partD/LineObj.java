/**
 * Created by jiy on 3/28/16.
 */
public class LineObj implements Comparable{
    private String name = "";
    private String line = "";

    public LineObj(String name, String line) {
        this.name = name;
        this.line = line;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    @Override
    public int compareTo(Object o) {
        //return line.compareTo(((LineObj)o).getLine());
        int thisLine = Integer.valueOf(line);
        int anotherLine = Integer.valueOf(((LineObj)o).getLine());
        return (thisLine - anotherLine);
    }

    @Override
    public String toString() {
        return "LineObj{" +
                "name='" + name + '\'' +
                ", line='" + line + '\'' +
                '}';
    }
}
