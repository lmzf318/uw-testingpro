/**
 * Created by Yuning on 3/13/16.
 */
public class Call {
    // current traverse level
    private int level = 0;
    // method's name
    private String name;

    public int getLevel() {
        return level;
    }

    public Call(int level, String name) {
        this.level = level;
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Call{" +
                "level=" + level +
                ", name='" + name + '\'' +
                '}';
    }
}
