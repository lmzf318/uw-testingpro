/**
 * Created by Yuning on 3/5/16.
 */
public class Constant {
    public static final int DEFAULT_SUPPORT = 3;
    public static final float DEFAULT_CONFIDENCE = 65f;

    public static final String NODE = "define";//"Call graph node";

    public static final String NODE_SCOPE = "@";
    public static final String NODE_SCOPE_END = "(";
    public static final int NODE_SCOPE_OFFSET = NODE_SCOPE.length();

    public static final String NODE_SCOPE_NULL = "<<";
    public static final int NODE_SCOPE_NULL_OFFSET = NODE_SCOPE_NULL.length();

    public static final String NODE_CALL = "call";
    public static final String NODE_CALL_POSITION = "@";
    public static final String NODE_CALL_POSITION_END = "(";
    public static final int NODE_CALL_POSITION_OFFSET = NODE_CALL_POSITION.length();

    // %2 = load i32* %i, align 4
    // call void @B(i32 %2, i32 1)
    public static final String NODE_ARG = "= load";
    public static final String NODE_ARG_DIV = "%";
    public static final int NODE_ARG_DIV_LEN = NODE_ARG_DIV.length();
    public static final String NODE_ARG_DIV_NUM_END = " =";
    public static final String NODE_ARG_DIV_ARG_END = ",";
    public static final String NODE_ARG_DIV_MUL_ARG_END = ")";

    // assign
    // store i32 0, i32* %i, align 4
    public static final String NODE_ASSIGN = "store";
    public static final String NODE_ASSIGN_DIV = "%";
    public static final String NODE_ASSIGN_DIV_END = ",";
    public static final int NODE_ASSIGN_DIV_LEN = NODE_ASSIGN_DIV.length();

    // define
    // %i = alloca i32, align 4
    public static final String NODE_DEF = "alloca";
    public static final String NODE_DEF_DIV = "%";
    public static final String NODE_DEF_DIV_END = " =";
    public static final int NODE_DEF_DIV_LEN = NODE_DEF_DIV.length();

    public static final String commonlyUsedMethods = "printf";

    public static final String CALL_PREFIX = "call_";
    public static final String DEF_PREFIX = "define_";
    public static final String ASSIGN_PREFIX = "assign_";
}
