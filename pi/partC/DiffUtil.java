import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by Yuning on 3/3/16.
 */
public class DiffUtil {
    private static HashMap<String, Integer> map = null;

    public static void findBugs(int defaultSupport, float defaultConfidence, HashMap<String, HashSet<String>> pairMap, CallGraph callGraph, int traverseLevel) {
        //deep find
        //DiffUtil.log("deep find", "before unfold: " + callGraph.toString());
        callGraph.unfoldMethods(traverseLevel);
        //DiffUtil.log("deep find", "after unfold: " + callGraph.toString());

        Iterator it = pairMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            // entry.getKey()
            // entry.getValue()
            String method = (String) entry.getKey();
            HashSet<String> pairSet = (HashSet<String>) entry.getValue();

            for (Iterator setIt = pairSet.iterator(); setIt.hasNext(); ) {
                String methodPair = (String) setIt.next();
                calculateBug(method, methodPair, callGraph, defaultSupport, defaultConfidence);
            }

        }
    }

    private static void calculateBug(String method, String methodPair, CallGraph callGraph,
                                     int defaultSupport, float defaultConfidence) {
        Pair pair = callGraph.findBugData(method, methodPair);
        // for example, for pair(A, B), singleA means number of appearances of A in call graph
        int singleA = pair.getSingleA();
        int singleB = pair.getSingleB();
        int support = pair.getPair();

        float confidenceA = support > 0 ? getConfidence(singleA, support) : 0;
        float confidenceB = support > 0 ? getConfidence(singleB, support) : 0;

        if (!Constant.commonlyUsedMethods.contains(method) &&
                singleA > 0 && support >= defaultSupport
                && confidenceA >= defaultConfidence) {
            printBug(support, method, methodPair, callGraph, confidenceA);
        }

        if (!Constant.commonlyUsedMethods.contains(methodPair) &&
                singleB > 0 && support >= defaultSupport
                && confidenceB >= defaultConfidence) {
            printBug(support, methodPair, method, callGraph, confidenceB);
        }
    }

    private static float getConfidence(int single, float support) {
        return ((float) (support) / (float) (support + single)) * 100f;
    }

    private static void printBug(int support,
                                 String method, String methodPair, CallGraph callGraph,
                                 float confidence) {
        HashSet<String> scopeSet = callGraph.getScopeList(method);
        for (Iterator scopeIt = scopeSet.iterator(); scopeIt.hasNext(); ) {
            String scope = (String) scopeIt.next();
            DiffUtil.log(method, methodPair, scope, support, confidence);
        }
    }

    public static void readStream(CallGraph callGraph) {
        Map<String, Integer> cgMap = new HashMap<>();

        InputStreamReader isReader = new InputStreamReader(System.in);
        BufferedReader bufReader = new BufferedReader(isReader);

        String inputStr = null;
        //HashMap<String, Integer> map = null;

        try {
            while ((inputStr = bufReader.readLine()) != null) {
                DiffUtil.handleInput(callGraph, inputStr);
            }
            isReader.close();
        } catch (Exception e) {
            DiffUtil.log("Exception", e.toString());
        }
    }

    /**
     * Analyze input string line by line and add scope/function call to call graph collection.
     *
     * @param callGraph store scope and call
     * @param inputStr  input String
     */
    public static void handleInput(CallGraph callGraph, String inputStr) {
        DiffUtil.log("Input readStream", inputStr);

        // goto a function
        if (inputStr.contains(Constant.NODE)) {
            // get scope
            String scope = DiffUtil.getScope(inputStr);
            DiffUtil.log("Input readStream scope", scope);

            // eliminate root scope
            if (scope == null) {
                map = null;
                return;
            }

            // start to generate a new call graph map
            map = new HashMap<>();

            callGraph.addFunctionMap(scope, map);
        }

        // call function in a scope
        if (inputStr.contains(Constant.NODE_CALL)) {
            String call = inputStr.substring(inputStr.indexOf(Constant.NODE_CALL)
                    + Constant.NODE_CALL_OFFSET, inputStr.length() - 1);
            DiffUtil.log("Input readStream call", call);
            if (map != null) {
                DiffUtil.log("Put map", call);
                // TODO increment value by 1 if we expand the function
                map.put(call, 1);
            }
        }
    }

    public static String getScope(String inputStr) {
        int index = 0;
        if ((index = inputStr.indexOf(Constant.NODE_SCOPE)) > -1) {
            //e.g. Call graph node for function: 'scope5'<<0x178ccc0>>  #uses=2
            return inputStr.substring(index + Constant.NODE_SCOPE_OFFSET, inputStr.lastIndexOf("\'"));
        } else if ((index = inputStr.indexOf(Constant.NODE_SCOPE_NULL)) > -1) {
            //e.g. Call graph node <<null function>><<0x178c8e0>>  #uses=0
            // INVALID!
            //return inputStr.substring(index + Constant.NODE_SCOPE_NULL_OFFSET, inputStr.indexOf(">>"));
            return null;
        } else {
            return null;
        }
    }

    /**
     * compare 2 Strings
     * to avoid hashcode collision, only compare Strings if the hashCode is equal
     **/
    public static boolean equals(final String srcStr, final String desStr) {
        return srcStr != null && desStr != null
                && srcStr.length() == desStr.length()
                && srcStr.hashCode() == desStr.hashCode()
                && srcStr.equals(desStr);
    }

    // check if a String is "" or null
    public static boolean isEmpty(String str) {
        return str == null || str.equals("");
    }

    // print result logs
    public static void log(String method, String methodPair, String scope, int support, float confidence) {
        String pairStr = method.compareTo(methodPair) < 0 ?
                (method + ", " + methodPair) : (methodPair + ", " + method);
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        String confidenceStr = df.format(confidence);
        System.out.println("bug: " + method + " in " + scope
                + ", pair: (" + pairStr + "), support: " + support
                + ", confidence: " + confidenceStr + "%");
    }

    // print out logs
    public static void log(String str) {
        //System.out.println(str);
    }

    // print out logs with tag
    public static void log(String tag, String content) {
        //System.out.println(tag + ": " + content);
    }
}
