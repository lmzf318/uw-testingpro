import java.util.*;

/**
 * Created by Yuning on 3/4/16.
 * <p/>
 * To store input call graphs and generate support and confidence
 */
public class CallGraph {
    private final static String TAG = "CallGraph";

    // For every function, add a HashMap into this Map after processing all functions.
    // Integer is for future expanding functions.
    private Map<String, HashMap<String, Integer>> cgMapCollection = new HashMap<>();

    // temporarily store a method's scopes
    private Map<String, HashSet<String>> bugMap = new HashMap<>();

    public CallGraph() {
        DiffUtil.log(TAG, "constructor");
    }

    public void addFunctionMap(String scope, HashMap<String, Integer> map) {
        this.cgMapCollection.put(scope, map);
    }

    public HashSet<String> getScopeList(String method) {
        return this.bugMap.get(method);
    }

    /**
     * Store (maybe) bug method and its corresponding scope in list.
     * While necessary, loop this list to print all scopes of this method
     */
    private void updateBugMap(String method, String scope) {
        HashSet<String> bugSet = null;
        if (bugMap.containsKey(method)) {
            bugSet = bugMap.get(method);
        } else {
            bugSet = new HashSet<>();
        }

        bugSet.add(scope);
        DiffUtil.log("Update BugMap", "method = " + method + ", scope = " + scope);
        this.bugMap.put(method, bugSet);
    }

    /**
     * add a scope's call set to a queue
     */
    private void addCall2Queue(LinkedList<String> queue, HashMap<String, Integer> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            queue.offer((String) entry.getKey());
        }
    }

    /**
     * Pairs like (A,B) are stored in pairMap.
     * Key is one call.
     * Value is set of this call's pair methods.
     */
    private void addCall2Map(String call, LinkedList<String> queue, HashMap<String, HashSet<String>> pairMap) {
        for (String pair : queue) {
            DiffUtil.log("Generate Pairs", "call = " + call + ", pair = " + pair);
            DiffUtil.log("Generate Pairs", "pairMap.get(call) = " + pairMap.get(call));
            DiffUtil.log("Generate Pairs", "pairMap.get(pair) = " + pairMap.get(pair));
            //if (DiffUtil.equals(pairMap.get(call), pair) || DiffUtil.equals(pairMap.get(pair), call)) {
            if ((pairMap.get(call) != null && pairMap.get(call).contains(pair))
                    || (pairMap.get(pair) != null && pairMap.get(pair).contains(call))) {
                DiffUtil.log("Generate Pairs", "pass!");
                continue;
            }

            HashSet<String> pairSet = pairMap.get(call);
            if (pairSet == null) {
                pairSet = new HashSet<>();
                pairMap.put(call, pairSet);
            }

            pairSet.add(pair);
        }
    }

    /**
     * add pairs to a map<call, calls>
     * For example, [A, B, C] in a scope is saved as <A={B, C}, B={C}>, which means pairs are A*{B,C} and B*{C}
     * */
    public void generatePairMap(HashMap<String, HashSet<String>> pairMap) {
        // DiffUtil.log("deep find", "before unfold: " + this.toString());
        // unfoldMethods(4);
        // DiffUtil.log("deep find", "after unfold: " + this.toString());

        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            HashMap<String, Integer> map = (HashMap<String, Integer>) entry.getValue();
            LinkedList<String> queue = new LinkedList<>();
            addCall2Queue(queue, map);
            DiffUtil.log("Queue Debug", queue.toString());
            for (String currentCall = queue.poll(); currentCall != null; currentCall = queue.poll()) {
                addCall2Map(currentCall, queue, pairMap);
            }
        }
    }

    /**
     * Traverse the whole call graph and find a)method, b)methodPair and c)pair
     * For p-1-a
     *
     * @param method     target method
     * @param methodPair method pair
     * @return a Pair with count of a, b and c, which means the appearance of method, methodPair, and pair
     */
    public Pair findBugData(String method, String methodPair) {
        // clear scopes every time finding a new bug
        // to eliminate previous result
        if (bugMap.containsKey(method)) {
            bugMap.remove(method);
        }
        if (bugMap.containsKey(methodPair)) {
            bugMap.remove(methodPair);
        }

        Pair pair = new Pair();
        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();

            HashMap<String, Integer> map = (HashMap<String, Integer>) entry.getValue();
            if (map.containsKey(method) && !map.containsKey(methodPair)) {
                pair.incrementSingelA();

                String scope = (String) entry.getKey();
                updateBugMap(method, scope);
            } else if (!map.containsKey(method) && map.containsKey(methodPair)) {
                pair.incrementSingelB();

                String scope = (String) entry.getKey();
                updateBugMap(methodPair, scope);
            } else if (map.containsKey(method) && map.containsKey(methodPair)) {
                pair.incrementPair();
            }
        }
        return pair;
    }

    // traverse methods in a call
        public void unfoldMethods(int traverseLevel) {
        // loop the whole call graph map and update it to a new one
        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            // entry.getKey()
            // entry.getValue()
            HashMap<String, Integer> map = (HashMap<String, Integer>) entry.getValue();

            LinkedList<Call> checkList = new LinkedList<>();

            addCall2CheckQueue(checkList, map, 0);

            for (Call currentCall = checkList.poll(); currentCall != null; currentCall = checkList.poll()) {
                HashMap<String, Integer> checkMap = this.cgMapCollection.get(currentCall.getName());
                DiffUtil.log(TAG, "Check method " + currentCall + "'s set: " + checkMap);

                if (currentCall.getLevel() <= traverseLevel) {
                    // current method's level is less than threshold
                    if ((checkMap != null && checkMap.size() > 1/*0*/ && currentCall.getLevel() < traverseLevel)) {
                        // remove this method
                        map.remove(currentCall.getName());
                        // add calls in its implementation to queue for later usage
                        // the traverse level of calls in this method's implementation should be incremented by currentCall.getLevel() + 1
                        addCall2CheckQueue(checkList, checkMap, currentCall.getLevel() + 1);
                    } else if (checkMap == null || (checkMap.size() == 0 || checkMap.size() == 1) || currentCall.getLevel() == traverseLevel) {
                        // checkMap == null/ checkMap.size() == 0: the method does NOT have its own calls (not a developer's code but C call)
                        // checkMap.size() == 1: the method only has one call in its implementation, make no sense
                        // currentCall.getLevel() == traverseLevel: traverse level reaches threshold
                        map.put(currentCall.getName(), 1);
                    }
                } else {
                    // current method's level is greater than threshold, do nothing
                    continue;
                }
            }
        }

    }

    private void addCall2CheckQueue(LinkedList<Call> queue, HashMap<String, Integer> map, int level) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String callName = (String) entry.getKey();
            // add new calls to check its call-sets iff this call has NOT been checked before
            Call call = new Call(level, callName);
            queue.offer(call);
        }
    }

    @Override
    public String toString() {
        return "CallGraph{" +
                "cgMapCollection=" + cgMapCollection +
                ", bugMap=" + bugMap +
                '}';
    }
}
