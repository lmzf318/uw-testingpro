/**
 * Created by Yuning on 3/8/16.
 */
public class Pair {
    private int singleA = 0;
    private int singleB = 0;
    private int pair = 0;

    public int getSingleA() {
        return singleA;
    }

    public int getSingleB() {
        return singleB;
    }

    public int getPair() {
        return pair;
    }

    public void incrementSingelA(){
        singleA++;
    }

    public void incrementSingelB(){
        singleB++;
    }

    public void incrementPair(){
        pair++;
    }
}
