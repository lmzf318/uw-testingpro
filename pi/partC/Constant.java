/**
 * Created by Yuning on 3/5/16.
 */
public class Constant {
    public static final int DEFAULT_SUPPORT = 3;
    public static final float DEFAULT_CONFIDENCE = 65f;

    public static final String NODE = "Call graph node";

    public static final String NODE_SCOPE = "for function: '";
    public static final int NODE_SCOPE_OFFSET = NODE_SCOPE.length();

    public static final String NODE_SCOPE_NULL = "<<";
    public static final int NODE_SCOPE_NULL_OFFSET = NODE_SCOPE_NULL.length();

    public static final String NODE_CALL = "calls function '";
    public static final int NODE_CALL_OFFSET = NODE_CALL.length();

    public static final String commonlyUsedMethods = "printf";
}
