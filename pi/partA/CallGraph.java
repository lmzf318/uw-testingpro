import java.util.*;

/**
 * Created by Yuning on 3/4/16.
 * <p/>
 * To store input call graphs and generate support and confidence
 */
public class CallGraph {
    private final static String TAG = "CallGraph";

    // For every function, add a HashMap into this Map after processing all functions.
    // Integer is for future expanding functions.
    private Map<String, HashMap<String, Integer>> cgMapCollection = new HashMap<>();

    // temporarily store a method's scopes
    private Map<String, HashSet<String>> bugMap = new HashMap<>();

    public CallGraph() {
        DiffUtil.log(TAG, "constructor");
    }

    public void addFunctionMap(String scope, HashMap<String, Integer> map) {
        this.cgMapCollection.put(scope, map);
    }


    public HashSet<String> getScopeList(String method) {
        return this.bugMap.get(method);
    }

    /**
     * Store (maybe) bug method and its corresponding scope in list.
     * While necessary, loop this list to print all scopes of this method
     */
    private void updateBugMap(String method, String scope) {
        HashSet<String> bugSet = null;
        if (bugMap.containsKey(method)) {
            bugSet = bugMap.get(method);
        } else {
            bugSet = new HashSet<>();
        }

        // TODO is t necessary to add updated list to map again?
        bugSet.add(scope);
        DiffUtil.log("Update BugMap", "method = " + method + ", scope = " + scope);
        this.bugMap.put(method, bugSet);
    }

    /**
     * add a scope's call set to a queue
     */
    private void addCall2Queue(LinkedList<String> queue, HashMap<String, Integer> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            queue.offer((String) entry.getKey());
        }
    }

    /**
     * Pairs like (A,B) are stored in pairMap.
     * Key is one call.
     * Value is set of this call's pair methods.
     */
    private void addCall2Map(String call, LinkedList<String> queue, HashMap<String, HashSet<String>> pairMap) {
        for (String pair : queue) {
            DiffUtil.log("Generate Pairs", "call = " + call + ", pair = " + pair);
            DiffUtil.log("Generate Pairs", "pairMap.get(call) = " + pairMap.get(call));
            DiffUtil.log("Generate Pairs", "pairMap.get(pair) = " + pairMap.get(pair));
            //if (DiffUtil.equals(pairMap.get(call), pair) || DiffUtil.equals(pairMap.get(pair), call)) {
            if ((pairMap.get(call) != null && pairMap.get(call).contains(pair))
                    || (pairMap.get(pair) != null && pairMap.get(pair).contains(call))) {
                DiffUtil.log("Generate Pairs", "pass!");
                continue;
            }

            HashSet<String> pairSet = pairMap.get(call);
            if (pairSet == null) {
                pairSet = new HashSet<>();
                pairMap.put(call, pairSet);
            }

            pairSet.add(pair);
        }
    }

    /**
     * add pairs to a map<call, calls>
     * For example, [A, B, C] in a scope is saved as <A={B, C}, B={C}>, which means pairs are A*{B,C} and B*{C}
     * */
    public void generatePairMap(HashMap<String, HashSet<String>> pairMap) {
        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            // entry.getKey()
            // entry.getValue()
            HashMap<String, Integer> map = (HashMap<String, Integer>) entry.getValue();
            LinkedList<String> queue = new LinkedList<>();
            addCall2Queue(queue, map);
            DiffUtil.log("Queue Debug", queue.toString());
            for (String currentCall = queue.poll(); currentCall != null; currentCall = queue.poll()) {
                addCall2Map(currentCall, queue, pairMap);
            }
        }
    }

    /**
     * Traverse the whole call graph and find method pairs
     *
     * @param method     target method
     * @param methodPair method pair
     * @return the count of method appears
     */
    public Pair findBugData(String method, String methodPair) {
        // clear scopes every time finding a new bug
        // to eliminate previous result
        if (bugMap.containsKey(method)) {
            bugMap.remove(method);
        }
        if (bugMap.containsKey(methodPair)) {
            bugMap.remove(methodPair);
        }

        Pair pair = new Pair();
        Iterator it = this.cgMapCollection.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            // entry.getKey()
            // entry.getValue()
            HashMap<String, Integer> map = (HashMap<String, Integer>) entry.getValue();
            if (map.containsKey(method) && !map.containsKey(methodPair)) {
                pair.incrementalSingelA();

                String scope = (String) entry.getKey();
                updateBugMap(method, scope);
            }else if(!map.containsKey(method) && map.containsKey(methodPair)){
                pair.incrementalSingelB();

                String scope = (String) entry.getKey();
                updateBugMap(methodPair, scope);
            }else if(map.containsKey(method) && map.containsKey(methodPair)){
                pair.incrementalPair();
            }
        }
        return pair;
    }

    @Override
    public String toString() {
        return "CallGraph{" +
                "cgMapCollection=" + cgMapCollection +
                ", bugMap=" + bugMap +
                '}';
    }
}
