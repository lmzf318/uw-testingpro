import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Yuning on 3/3/2016.
 */
public class Analyzer {

    public static void main(String[] args) {
        // record time stamp for checking performance
        long time1 = System.currentTimeMillis();
        long time2;
        // set threshold of scope and confidence
        int defaultSupport = Constant.DEFAULT_SUPPORT;
        float defaultConfidence = Constant.DEFAULT_CONFIDENCE;

        DiffUtil.log("Input Arguments", "Length is " + args.length);
        switch (args.length) {
            case 0:
                // no arguments input
                DiffUtil.log("Input Arguments", "Use default arguments.");
                break;
            case 2:
                defaultSupport = Integer.valueOf(args[0]);
                defaultConfidence = Float.valueOf(args[1]);
                DiffUtil.log("Input Arguments", "support = " + defaultSupport + ", confidence = " + defaultConfidence);
                break;
            default:
                // invalid arguments input
                // exit directly
                DiffUtil.log("Input Arguments", "Invalid arguments. Exit now.");
                return;
        }

        // save call graph to HashMap
        CallGraph callGraph = new CallGraph();
        DiffUtil.readStream(callGraph);
        //DiffUtil.log("Generated Map", callGraph.toString());
        time2 = System.currentTimeMillis();
        DiffUtil.log("Timeout1: " + (time2-time1));

        // get method pairs
        HashMap<String, HashSet<String>> pairMap = new HashMap<>();
        callGraph.generatePairMap(pairMap);
        //DiffUtil.log("Pair Map", pairMap.toString());
        time2 = System.currentTimeMillis();
        DiffUtil.log("Timeout2: " + (time2-time1));

        // try to find may beliefs
        DiffUtil.findBugs(defaultSupport, defaultConfidence, pairMap, callGraph);
        time2 = System.currentTimeMillis();
        DiffUtil.log("Timeout3: " + (time2-time1));
    }
}
